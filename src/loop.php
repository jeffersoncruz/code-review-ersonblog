<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
	<article class="article-block" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<!-- post title -->

		<div class="date">
			<time datetime="<?php the_time('Y-m-d'); ?> <?php the_time('H:i'); ?>">
				<div class="day"><?php the_time('j'); ?></div> <!-- kahit div nalang para di na mag display:block -->
				<span class="month-year"><?php the_time('F Y'); ?></span> <!-- kahit div nalang para di na mag display:block -->
			</time>
		</div>

		<div class="head-wrap">
			<h2>
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
			</h2>
			<!-- /post title -->

			<!-- post details -->

			<p class="meta">
				<span>
					<?php _e( 'by', 'html5blank' ); ?>	
				</span>
				<span class="author">
					<?php the_author_posts_link(); ?>
				</span>
				<span>
					<a class="link" target="_blank" href="<?php echo get_the_author_meta( 'user_url' ); ?>"><?php echo '- ' . get_the_author_meta( 'user_url' ); ?></a>
				</span>
			</p>			
		</div>



		<!-- /post details -->

	

		<!-- post thumbnail -->
		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				<?php the_post_thumbnail('medium'); // Declare pixel size you need inside the array ?>
			</a>
		<?php endif; ?>

		<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>			
		<!-- /post thumbnail -->



		<?php //edit_post_link(); ?>

	</article>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
