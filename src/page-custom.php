
<?php
/**
 * Template Name: custom_page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<?php get_header(); ?>

	<main  role="main" aria-label="Content" id="main">
		<!-- section -->
		<section>


		<?php $loop = new WP_Query( array( 'post_type' => 'post_custom',
       'posts_per_page' => 5)
           );
       while ( $loop->have_posts() ) : $loop->the_post(); ?>


        <h1><?php the_field('name'); ?></h1>
        <img src="<?php the_field('upload'); ?>" alt="">

       <?php endwhile; wp_reset_query(); ?>



			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
