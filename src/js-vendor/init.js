function runCanvas() {
  var world;
  var CANVAS_WIDTH  = 1300,CANVAS_HEIGHT = 600,SCALE = 30;
  var b2Vec2 = Box2D.Common.Math.b2Vec2;

  var canvas = document.getElementById('canvas');
  var context = canvas.getContext('2d'),
      b2Vec2 = Box2D.Common.Math.b2Vec2,
      b2BodyDef = Box2D.Dynamics.b2BodyDef,
      b2Body = Box2D.Dynamics.b2Body,
      b2FixtureDef = Box2D.Dynamics.b2FixtureDef,
      b2Fixture = Box2D.Dynamics.b2Fixture,
      b2World = Box2D.Dynamics.b2World,
      b2MassData = Box2D.Collision.Shapes.b2MassData, 
      b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape, 
      b2CircleShape = Box2D.Collision.Shapes.b2CircleShape, 
      b2DebugDraw = Box2D.Dynamics.b2DebugDraw;

      world = new b2World( 
              new b2Vec2(0, 60), //gravity
              false //allowsleep
      );
  // init 
  function init() {
    var fixDef = new b2FixtureDef;

    var bodyDef = new b2BodyDef;

    // ----- Ground -------//
    bodyDef.type = b2Body.b2_staticBody;
    bodyDef.position.x = CANVAS_WIDTH / 2 / SCALE;
    bodyDef.position.y = CANVAS_HEIGHT / SCALE;
    fixDef.shape = new b2PolygonShape;
    fixDef.shape.SetAsBox((CANVAS_WIDTH / SCALE) / 2, (8 / SCALE) / 2);
    world.CreateBody(bodyDef).CreateFixture(fixDef);

    //----- Right Wall -----//
    bodyDef.type = b2Body.b2_staticBody;
    bodyDef.position.x = CANVAS_WIDTH / SCALE;
    bodyDef.position.y = CANVAS_HEIGHT / SCALE;
    fixDef.shape = new b2PolygonShape;
    fixDef.shape.SetAsBox((8 / SCALE) / 2, CANVAS_HEIGHT / SCALE) / 2;
    world.CreateBody(bodyDef).CreateFixture(fixDef);

    // ----- Left Wall ----- //
    bodyDef.type = b2Body.b2_staticBody;
    bodyDef.position.x = 0;
    bodyDef.position.y = CANVAS_HEIGHT / SCALE;
    fixDef.shape = new b2PolygonShape;
    fixDef.shape.SetAsBox((8 / SCALE) / 2, CANVAS_HEIGHT / SCALE);
    world.CreateBody(bodyDef).CreateFixture(fixDef);

    // .. create some objects
    bodyDef.type = b2Body.b2_dynamicBody;

    // .. setup debug draw
    var debugDraw = new b2DebugDraw();
        debugDraw.SetSprite(document.getElementById("canvas").getContext("2d"));
        debugDraw.SetDrawScale(30.0);
        debugDraw.SetFillAlpha(0.6);
        debugDraw.SetLineThickness(0);
        debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
        world.SetDebugDraw(debugDraw);

    window.setInterval(update, 1000 / 50);
  };

  function update() {
    world.Step(
      1 / 120, // frame rate
      10 ,     // velocity iterations
      10       // position iterations
    );

    context.clearRect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);

    // for shape color
    // world.DrawDebugData();

    // draw image
    for (b = world.GetBodyList() ; b; b = b.GetNext()) {
      if (b.GetType() == b2Body.b2_dynamicBody) {
          var pos = b.GetPosition();
          imageWidth = b.m_userData.imageW;
          imageHeight = b.m_userData.imageH;
          var imgObj = new Image(imageWidth,imageHeight);
          imgObj.src = b.m_userData.image;
          context.save();
          context.translate(pos.x * SCALE, pos.y * SCALE);
          context.rotate(b.GetAngle());
          context.drawImage(imgObj,-1 * imageWidth / 2,-1 * imageHeight / 2);
          context.restore();
      }
    }

    world.ClearForces();
  };

  init();

  // create image
  function addThisImage(id,x,y,imageFileName,imageWidth,imageHeight,shape) {
    var imgObj = new Image();
    imgObj.src = imageFileName;
    context.drawImage(imgObj,x,y);
    var bodyDef = new b2BodyDef;
    bodyDef.type = b2Body.b2_dynamicBody;
    var fixDef = new b2FixtureDef;
    if (shape == 'box') {
      fixDef.shape = new b2PolygonShape;
      fixDef.shape.SetAsBox(                 
        imageWidth / 2 / SCALE,
        imageHeight / 2 / SCALE
      );
    } else {
      fixDef.shape = new b2CircleShape(
        imageWidth / 2 / SCALE
      );
    }
    
    
    bodyDef.position.x = x;
    bodyDef.position.y = y;
    bodyDef.userData = {
      image: imageFileName,
      whatever:"unselected",
      imageW: imageWidth,
      imageH: imageHeight,
      id: id
    };

    fixDef.density = -1;
    fixDef.friction = 0.3;
    fixDef.restitution = 0.1; //colission

    world.CreateBody(bodyDef).CreateFixture(fixDef);
  }
  // end of runCanvas();
    for(var i = 0; i < 10; ++i) {
      addThisImage('adobe',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/adobe.png',45,45,'circle');
      addThisImage('adc',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/adc.png',45,45,'circle');
      addThisImage('awards',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/awards.png',45,45,'circle'); 
      addThisImage('commarts',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/commarts.png',45,45,'circle');
      addThisImage('fwa',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/fwa.png',45,45,'circle');
      addThisImage('oneshow',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/oneshow.png',45,45,'circle');
      addThisImage('pixel',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/pixel.png',45,45,'circle');
      addThisImage('sandbox',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/sandbox.png',45,45,'circle');
      addThisImage('sxsw',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/sxsw.png',45,45,'circle');
      addThisImage('vimeo',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/vimeo.png',45,45,'circle');
      addThisImage('votd',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/votd.png',45,45,'circle');
      addThisImage('webby',Math.random() * 4 /*upperbound-lowerbound*/ + 13/*lowerbound*/,Math.random() * -100,'../wp-content/themes/ersonblog/src/img/webby.png',45,45,'circle');         
    }


}



