<?php get_header(); ?>

	<main  role="main" aria-label="Content" id="main">
		<!-- section -->
		<section>


			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</main>

<?php //get_sidebar(); ?>

<?php //echo do_shortcode("[sample]"); ?>

<?php //dynamic_sidebar('widget-area-2'); ?>

<?php //sample_do_action(); ?>


<?php get_footer(); ?>
