
<?php
/**
 * Template Name: custom_legwork
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<?php //get_header(); ?>
<?php wp_head(); ?>









		<div class="mainwrap js-wheel">

			<!-- content -->
			<!-- <a href="javascript:void(0)" onclick="popOpen('custom')">Trigger popup</a> -->

			<div class="sidebar-wrap">
				<div class="sidebar">
					<a class="js-btnsidenav side-nav active" href="javascript:void(0)">
						<div class="indicator">
							<div class="tag">
								<img class="sidenav-logo" src="<?php echo get_template_directory_uri(); ?> /img/twitter.svg" alt="">
							</div>
							<div class="bar"></div>
						</div>
						<div class="label">Animation Reel</div>
					</a>
					
					<?php $count = 1; ?>

					<?php $loop = new WP_Query( array( 'post_type' => 'post_custom',
					   'posts_per_page' => 5, 'order' => 'ASC' )
					       );

					   while ( $loop->have_posts() ) : $loop->the_post(); ?>
					   

						<a class="js-btnsidenav side-nav" href="javascript:void(0)">
							<div class="indicator">
								<div class="tag">
									<?php echo "0" .$count ?>
								</div>
								<div class="bar"></div>
							</div>
							<div class="label"><?php the_title(); ?></div>
						</a>
					   
					   <?php $count += 1; ?>
					<?php endwhile; wp_reset_query(); ?>
				</div>				
			</div>





			<!-- legwork nav start-->
			<div class="content" style="background-color:green">
				<div class="section">logo</div>

				<?php $count_section = 1; ?>

				<?php $loop = new WP_Query( array( 'post_type' => 'post_custom',
				   'posts_per_page' => 5, 'order' => 'ASC' )
				       );
				   while ( $loop->have_posts() ) : $loop->the_post(); ?>
					
					<div class="section" style="background-image:url('<?php the_field('background') ?>');">

					<?php 
						if ($count_section == 3) {
					?>
						<div class="canvas-wrap">
							<canvas id="canvas" class="canvas" width="1300" height="600" ></canvas>						
						</div>
					
					<?php } ?>

							<div class="description-left">
								<h2> <?php the_title(); ?></h2>
						       	<h1> <?php the_field('title'); ?></h1>
								<?php the_field('description'); ?>	
							</div>
							
					</div>

				   <?php $count_section += 1; ?>
				<?php endwhile; wp_reset_query(); ?>
			</div>
			<!-- legwork nav end -->


<script>
	
</script>



		</div> <!-- end mainwrap -->
<?php //get_footer(); ?>
