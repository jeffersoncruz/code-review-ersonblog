			<!-- footer -->
			<footer class="footer" role="contentinfo">


				<a class="logo" href="<?php echo home_url(); ?>">
					<img src="<?php logo_image(); ?>" alt="">
				</a>

				<div class="footer-wrap">
					<?php footer_nav(); ?>				

					<p class="copyright">
						<?php copyright_text(); ?>
						
					</p>					
				</div>



				<img id="upload_customize_img" src="<?php upload_customize_img(); ?>" alt="">

				<?php //upload_customize_media(); ?>

				


			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<!-- <?php wp_footer(); ?> -->

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
